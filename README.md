# Сервис возведения в степень

### Требования
- PHP 7.4


### Установка

```
$ composer require ivansochkov/otus-composer-package
```

### Использование

```
<?php
$exp = new ExponentiationService();
echo $exp->Exponentiation(2,2);   // 4
```