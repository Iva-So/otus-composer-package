<?php

declare(strict_types=1);

namespace IvanSochkov\OtusComposerPackage\Services;

class ExponentiationService
{
    public function Exponentiation(int $number, int $degree): int
    {
        return $number ** $degree;
    }
}
